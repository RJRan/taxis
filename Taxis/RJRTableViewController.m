//
//  RJRTableViewController.m
//  Taxis
//
//  Created by Robert Randell on 26/03/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRTableViewController.h"
#import "RJRDetailViewController.h"
#import "RJRTaxiObject.h"

@interface RJRTableViewController ()

@end

@implementation RJRTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    RJRTaxiObject *taxiEntry = [[RJRTaxiObject alloc] init];
    taxiEntry.rankStreet = @"Street";
    taxiEntry.opHours = @"Hours";
    taxiEntry.taxiSpaces = 0;
    
    NSURL *taxiURL = [NSURL URLWithString:@"http://www.apsgroup-studio.co.uk/devdatasources/taxi.json"];
    NSData *jsonData = [NSData dataWithContentsOfURL:taxiURL];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                   options:0
                                                                     error:&error];
    //NSLog(@"%@", dataDictionary);
    
    self.taxiInfo = [NSMutableArray array];
    
    NSArray *taxiInfoArray = [dataDictionary objectForKey:@"Taxies"];
    
    for (NSDictionary *taxiInfoDictionary in taxiInfoArray) {
        RJRTaxiObject *ti = [RJRTaxiObject taxiObjectWithStreet:[taxiInfoDictionary objectForKey:@"Street"]];
        ti.opHours = [taxiInfoDictionary objectForKey:@"Operational hours"];
        ti.locationGPS = [taxiInfoDictionary objectForKey:@"Location"];
        ti.streetName = [taxiInfoDictionary objectForKey:@"Street"];
        ti.taxiSpaces = [[taxiInfoDictionary objectForKey:@"Number of  spaces"] doubleValue];
        
        [self.taxiInfo addObject:ti];
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.taxiInfo count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    RJRTaxiObject *to = [self.taxiInfo objectAtIndex:indexPath.row];
    
    NSArray *taxiName = [to.rankStreet componentsSeparatedByString:@","];
    NSString *streetName = [taxiName objectAtIndex:0];
    
    cell.textLabel.text = streetName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.0f taxis", to.taxiSpaces];
    return cell;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ( [segue.identifier isEqualToString:@"taxiDetails"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        RJRTaxiObject *taxiDetails = [self.taxiInfo objectAtIndex:indexPath.row];
        RJRDetailViewController *dvc = (RJRDetailViewController *)segue.destinationViewController;
        dvc.location = taxiDetails.locationGPS;
        dvc.taxiName = taxiDetails.streetName;
        
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
