//
//  RJRTaxiObject.h
//  Taxis
//
//  Created by Robert Randell on 26/03/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RJRTaxiObject : NSObject

@property (nonatomic, strong) NSString *rankStreet;
@property (nonatomic, strong) NSString *opHours;
@property (nonatomic, strong) NSString *locationGPS;
@property (nonatomic, strong) NSString *streetName;
@property (nonatomic) double taxiSpaces;

-(id)initWithStreet:(NSString *)rankStreet;
+(id)taxiObjectWithStreet:(NSString *)rankStreet;

@end
