//
//  RJRTaxiObject.m
//  Taxis
//
//  Created by Robert Randell on 26/03/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRTaxiObject.h"

@implementation RJRTaxiObject

-(id)initWithStreet:(NSString *)rankStreet {
    
    self = [super init];
    
    if (self) {
        _rankStreet = rankStreet;
    }
    return self;
}

+(id)taxiObjectWithStreet:(NSString *)rankStreet {
    
    return [[self alloc] initWithStreet:rankStreet];
    
}

@end
