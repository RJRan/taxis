//
//  RJRDetailViewController.m
//  Taxis
//
//  Created by Robert Randell on 26/03/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRDetailViewController.h"

@interface RJRDetailViewController ()

@end

@implementation RJRDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [_mapTaxiRanks setDelegate:self];
    
    NSArray *taxiName = [self.taxiName componentsSeparatedByString:@","];
    NSString *street = [taxiName objectAtIndex:0];
    
    NSArray *mapLocation = [self.location componentsSeparatedByString:@","];
    double latitude = [[mapLocation objectAtIndex:0] doubleValue];
    double longitude = [[mapLocation objectAtIndex:1] doubleValue];
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(latitude, longitude);
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coord];
    [annotation setTitle:street];
    [_mapTaxiRanks addAnnotation:annotation];
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = latitude;
    newRegion.center.longitude = longitude;
    newRegion.span.latitudeDelta = 0.005;
    newRegion.span.longitudeDelta = 0.005;
    
    [_mapTaxiRanks setRegion:newRegion animated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
