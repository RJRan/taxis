//
//  RJRDetailViewController.h
//  Taxis
//
//  Created by Robert Randell on 26/03/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface RJRDetailViewController : UIViewController <MKMapViewDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *mapTaxiRanks;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *taxiName;

@end
